import requests
import bs4
import subprocess
import time
from tqdm import tqdm
'''videoDownloadTs.py,本程式可以使用'''
'''下載線上影片.ts結尾檔案'''

def main():
    '''主程式'''
    '''使用者輸入下載的首點位址與尾點位址'''
    httpHead = input('請輸入首段ts網址: ')
    httpTail = input('請輸入末段ts網址: ')
    
    ''' test values
    
    httpHead = 'https://cdn-3.kkp2p.com/hls/2020/05/21/t4woj8OU/out001.ts'
    httpTail = 'https://cdn-3.kkp2p.com/hls/2020/05/21/t4woj8OU/out007.ts'
    httpHead = 'https://zk.sd-dykj.com/2020/08/10/eeY8uUmExGgRA4J5/out001.ts'
    httpTail = 'https://zk.sd-dykj.com/2020/08/10/eeY8uUmExGgRA4J5/out363.ts'
    end '''
    
    '''呼叫各涵式並執行結果'''
    startTime = int(time.time())
    try:
        indexGOT, ListHeadGot, ListTailGot = getIndex(httpHead.strip(),httpTail.strip())
        tailGOT = getTail(ListHeadGot, ListTailGot,httpTail)
        headGOT = getHead(httpHead.strip(),httpTail.strip(),indexGOT)
        countNumGOT = getCountNum(headGOT,tailGOT,httpTail.strip())
        lenMiddleGOT = getMiddleLen(httpTail,headGOT,tailGOT)
        videoGOTts = getVideo(headGOT,ListHeadGot, countNumGOT,tailGOT, lenMiddleGOT,ListTailGot)
        endTime = int(time.time())
        print(videoGOTts,"花費時間{}秒".format(endTime-startTime),sep='\n')
    except:
        print('請檢查輸入的網址')

def getVideo(H,listHeadUrl,counter,T,Mlen,listTailUrl):
    '''自動改變下載位置,下載影片, 並從.ts 轉檔到 .mp4 儲存'''
    httpSourceIn = '.\\dorama.ts'
    convertVideo = '.\\dorama.mp4'
    with open(httpSourceIn,'wb') as f:
        lenNum = len(H)
        midFirstIndex = listHeadUrl[lenNum] #變動區塊前端的不變數,通常是0,所以不應該變動
        checkMidFirstIndex = listTailUrl[lenNum] #人工index數字有否對齊檢查用; 正確可用
        midPart = ''
        for num in range(counter+1):
            num = str(num)
            lenNum = len(num)
            frontPartIntNum = Mlen - lenNum
            frontPartValue = midFirstIndex * frontPartIntNum
            midPart = frontPartValue + num
            url = H + midPart + T
            print(url)
            req = requests.get(url,stream=True)
            for i in req.iter_content(chunk_size=1024):
                f.write(i)
    subprocess.run(['C:\\Users\\river\\python\\mpeg\\ffmpeg-20200417-889ad93-win64-static\\bin\\ffmpeg','-i',httpSourceIn,convertVideo])
    report = '完成'
    return report

def getMiddleLen(tt,hList,tList):
    '''尋找變動值內值的個數'''
    testWhole = len(tt)
    testHead = len(hList)
    testTail = len(tList)
    return testWhole-testHead-testTail

def getCountNum(headpart,tailPart,tt):
    '''尋找最後一個變數的值,當作迴圈的計數器'''
    fullUrl = tt
    headUrl = len(headpart)
    secondPart = fullUrl[headUrl:]
    middle = secondPart.split('.')
    num = int(middle[0])
    return num

def getIndex(hh,tt):
    '''檢測兩個網址輸入的長度要一樣; 獲得index'''
    if len(hh) == len(tt):
        getIndex = len(hh)
        getListHead = list(hh)
        getListTail = list(tt)
    else:
        print('兩個網址的長度不同')
    return getIndex, getListHead, getListTail

def getTail(hh,tt,strTail):
    '''網址末端不變的的檔案格式剪下來;例如本例子是: .ts'''
    tailList = strTail.split('.')
    tail = '.'+tailList[-1]

    return tail

def getHead(hh,tt,idx):
    '''根據輸入的兩個網址, 把網址前半段不變的部分剪下來'''
    HEAD = ''
    for letter in range(idx):
        if hh[letter] == tt[letter]:
            HEAD += hh[letter]
        else:
            break
    return HEAD

if __name__=='__main__':
    '''啟動主程式'''
    main()

'''
把轉檔寫成函數,但多這一個轉檔函數要花費更多時間下載, 本例, (獨立函數)24秒 vs (無獨立函數)20秒, 4秒的差距 
def convertMP4(inTS):
    convertVideo = 'dorama1.mp4'
    subprocess.run(['C:\\Users\\river\\python\\mpeg\\ffmpeg-20200417-889ad93-win64-static\\bin\\ffmpeg',
                '-i',inTS,convertVideo])
    report = '完成'
    return report
'''