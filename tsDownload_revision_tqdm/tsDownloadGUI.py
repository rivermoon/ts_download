import tsGetImport as ts
from tkinter import *
import time
# from tkinter.filedialog import asksaveasfilename

''' test values'''
# httpHead = 'https://cdn-1.okp2pweb.com/hls/2020/05/03/9KSrjnUH/out000.ts'
# httpTail = 'https://cdn-1.okp2pweb.com/hls/2020/05/03/9KSrjnUH/out030.ts' 
'''end '''

def downloadTs():
    startTime = int(time.time())
    httpHead = e1.get().strip()
    httpTail = e2.get().strip()
    if len(httpHead) == len(httpTail):
        getIndex = len(httpHead)
        getListHead = list(httpHead)
        getListTail = list(httpTail)
        # fileNameMP4=asksaveasfilename(defaultextension='.mp4')
        fileNameMP4 = ''
        messageShow()
        msg1, msg2, msgM = ts.main(getIndex,getListHead,getListTail, fileNameMP4,httpTail,httpHead)
        endTime = int(time.time())
        usedTotalSecond = endTime - startTime
        usedHour = usedTotalSecond // 60 // 60
        usedMinute = (usedTotalSecond // 60) % 60
        usedSecond = usedTotalSecond % 60
        usedTime = "花費時間: {}時{}分{}秒".format(usedHour,usedMinute,usedSecond)
        if msgM == 'ok':
            lab5['text']=msg1
            lab6['text']=msg2
            lab7['text']=usedTime
        else:
            lab5['text']=msg1
            lab6['text']=msg2
            lab7['text']=''
    else:
        message1 = '兩個網址的長度不同'
        message2 = '請重新輸入'
        lab5['text']=message1  
        lab6['text']=message2
    lab4 = Label(tk,text='執行結果',fg='white',bg='black',font='mingliu 14 bold',width=10)
    lab4.grid(row=11,column=1,pady=5)

def messageShow():
    lab1['text']='請稍後,程式執行中...'

tk = Tk()
tk.title('.ts檔案下載程式')
tk.geometry('900x600')
tk.config(bg='orange')
scrollbar = Scrollbar(tk)
labTop = Label(tk,text='ts影片的第一載點: ',font=15).grid(row=0,padx=15,pady=5)
labBottom = Label(tk, text='ts影片的末端載點: ',font=15).grid(row=1,padx=15,pady=5)
e1=Entry(tk,width=90)
e2=Entry(tk,width=90)
e1.insert(1,'https://cdn-3.kkp2p.com/hls/2020/05/21/t4woj8OU/out001.ts')
e2.insert(1,'https://cdn-3.kkp2p.com/hls/2020/05/21/t4woj8OU/out007.ts')
e1.grid(row=0,column=1,pady=5)
e2.grid(row=1,column=1,pady=5)
'''display messages'''
intro = '''這是我的python作品, 你可以試用, 但請勿濫用. 你需要輸入.ts\
結尾的網址.\n
在網頁中的影片按播放鍵前,先開啟開發者介面(F12), 再點擊Network, 然後按\n
下撥放鍵觀察流動的檔案中會出現.ts結尾的檔案,這時影片應該已經在網頁中撥\n
放了,這就是你的初端載點, 接著點擊,一旁視窗會出現完整的載點網址, 複製貼\
上.\n
接著把影片快速調到結束, 觀察影片結束時.ts檔案的位置,重複同樣步驟並複製\n
貼上,這是載點的末端網址,下載完成後請自行下載撥放軟體.'''
text = Text(tk, height=6, width=90)
text.insert(END,intro)
text.grid(row=8,column=1)
scrollbar = Scrollbar(tk,command=text.yview)
scrollbar.grid(row=8,column=1,sticky='ens')
text.config(yscrollcommand=scrollbar.set)
lab1 = Label(tk,fg='blue',bg='orange',font='mingliu 14 bold',width=55)
lab1.grid(row=10,column=1,pady=50)
lab5 = Label(tk,fg='blue',bg='orange',font='mingliu 14 bold',width=55)
lab5.grid(row=12,column=1,pady=5)
lab6 = Label(tk,fg='blue',bg='orange',font='mingliu 14 bold',width=55)
lab6.grid(row=13,column=1,pady=5)
lab7 = Label(tk,fg='blue',bg='orange',font='mingliu 14 bold',width=55)
lab7.grid(row=14,column=1,pady=5)
'''end messages'''
btn1= Button(tk,text='下載',width=5,font=12,command=downloadTs).grid(row=2)
btn2 = Button(tk,text='結束',width=5,font=12,command=tk.destroy).grid(row=2,column=1,pady=30)
tk.mainloop()